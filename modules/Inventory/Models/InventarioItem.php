<?php

namespace Modules\Inventory\Models;

use App\Models\Tenant\Item;
use App\Models\Tenant\ModelTenant;

class InventarioItem extends ModelTenant
{ 
    protected $table = 'inventario_items';

    protected $fillable = [
        'inventario_id',
        'item_id',
        'item',
        'quantity',
        'unit_value',

        'affectation_igv_type_id',
        'total_base_igv',
        'percentage_igv',
        'total_igv',

        'system_isc_type_id',
        'total_base_isc',
        'percentage_isc',
        'total_isc',

        'total_base_other_taxes',
        'percentage_other_taxes',
        'total_other_taxes',
        'total_taxes',

        'price_type_id',
        'unit_price',

        'total_value',
        'total',

        'attributes',
        'charges',
        'lot_code',
        'warehouse_id',
        'discounts',
        'date_of_due'
    ];

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
    public function inventarioitem()
    {
        return $this->belongsTo(inventarioitem::class);
    }
}